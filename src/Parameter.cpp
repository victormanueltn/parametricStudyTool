#include "Parameter.h"
#include <string>
#include <vector>
#include <iostream>
#include <fstream>
#include <algorithm>
#include <cstdlib>


Parameter::Parameter(void)
{
}


Parameter::~Parameter(void)
{
}

void Parameter::openFile( std::ifstream& stream, const std::string& fileName ) const
{
	stream.open(fileName.c_str());
	if (stream.fail())
	{
		std::cerr << "Error: File " << fileName << " could not be opened." << std::endl;
		exit (EXIT_FAILURE);
	}
}

void Parameter::openFile( std::ofstream& stream, const std::string& fileName ) const
{
	stream.open(fileName.c_str());
	if (stream.fail())
	{
		std::cerr << "Error: File " << nameOfOriginalFile << " could not be opened." << std::endl;
		exit (EXIT_FAILURE);
	}
}

Parameter::Parameter( const std::string& parameterFile )
{
	std::ifstream input;
	openFile(input, parameterFile);
	

	input >> name;
	input >> nameOfOriginalFile;
	input >> nameOfTargetFile;


	std::cout << "Creating parameter: " << std::endl;
	std::cout << name << std::endl;
	std::cout << "originally in file: " << std::endl;
	std::cout << nameOfOriginalFile << std::endl;
	std::cout << "and to be written in file(s): " << std::endl;
	std::cout << nameOfTargetFile << std::endl ;
	

	std::string line;
	getline( input, line );
	while ( getline( input, line ) )
	{
		// Eliminating possible new lines
		line.erase(std::remove(line.begin(), line.end(), '\n'), line.end());
		line.erase(std::remove(line.begin(), line.end(), '\r'), line.end());

		this->listOfValues.push_back(line);
	}


	std::cout << "Finding out line number in original file." << std::endl;
	findLineNumber(nameOfOriginalFile);
	std::cout << "Parameter " << name << " successfully created" << std::endl << std::endl;

	input.close();
}



void Parameter::findLineNumber(const std::string& nameOfOriginalFile)
{
	std::ifstream originalFile;
	openFile(originalFile, nameOfOriginalFile);

	std::string line;
	size_t position = std::string::npos;
	int currentLineNumber = 0;
	while ( getline( originalFile, line ) )
	{
		position = line.find(this->name);
		if (position != std::string::npos)
		{
			lineNumber = currentLineNumber;
			initialValue = line;
			break;
		}
		currentLineNumber++;
	}
	if ( position == std::string::npos )
	{
		std::cerr << "Error: parameter " << name << " not found in file: " << std::endl;
		std::cerr << nameOfOriginalFile << std::endl;
		exit (EXIT_FAILURE);
	}
}



void Parameter::affectParameterValueInTargetFile(const std::string& value) const
{
		std::ifstream originalFile;
		std::ofstream targetFile;

		openFile(originalFile, nameOfOriginalFile);
		openFile(targetFile, nameOfTargetFile);

		std::string line;
		int currentLineNumber = 0;
		while ( getline( originalFile, line ) )
		{
			if ( currentLineNumber != lineNumber )
				targetFile << line << std::endl;
			else
				targetFile << value << std::endl;

			currentLineNumber++;
		}

		originalFile.close();
		targetFile.close();
}

std::string Parameter::getValue(const int& i) const
{
	return listOfValues[i];
}

int Parameter::numberOfValues() const
{
	return listOfValues.size();
}

std::string Parameter::getName() const
{
	return name;
}
