#include "ParameterCreator.h"


ParameterCreator::ParameterCreator(void)
{
}

ParameterCreator::ParameterCreator( const std::string& configFile )
{

	std::ifstream input;
	openFile(input, configFile);
	
	input >> name;
	input >> waitTimeInMinutesBetweenBatches;
	input >> waitTimeInMinutesBetweenSimulations;
	std::string outputDirectoryParameter;
	input >> outputDirectoryParameter;
	setOutputParameter(outputDirectoryParameter);
	 

	std::string line;
	getline( input, line );
	std::cout << "ignored line " << line << std::endl;
	while ( getline( input, line ) )
	{
		// Eliminating possible new lines
		line.erase(std::remove(line.begin(), line.end(), '\n'), line.end());
		line.erase(std::remove(line.begin(), line.end(), '\r'), line.end());
		
		this->addParameter(Parameter(line));
		std::cout << "line " << line << std::endl;
	}
	std::cout << "Outside of while loop" << std::endl;

	createSimpleSetOfSimulations();
	std::cout << "Successfully created set of simulations to be launched." << std::endl;
	launchSimulations();
	writeReport();

}



ParameterCreator::~ParameterCreator(void)
{
}

void ParameterCreator::openFile( std::ifstream& stream, const std::string& fileName) const
{
	stream.open(fileName.c_str());
	if (stream.fail())
	{
		std::cerr << "Error: File " << fileName << " could not be opened." << std::endl;
		exit (EXIT_FAILURE);
	}
}

void ParameterCreator::addParameter(Parameter p)
{
	parameters.push_back(p);
}


void ParameterCreator::augmentValueIndexes( std::vector<size_t>& valueIndexes, const int& parameterIndex ) const
{
	if ( parameterIndex >= 0 )
	{ 
		if ( valueIndexes[parameterIndex] + 1 < parameters[parameterIndex].numberOfValues() )
		{
			valueIndexes[parameterIndex]++;	
		}
		else
		{
			valueIndexes[parameterIndex] = 0;
			augmentValueIndexes(valueIndexes, parameterIndex - 1);
		}
	}
}

void ParameterCreator::calculateNumberOfSimulations() 
{
	numberOfSimulations = 1;	
	for ( size_t i = 0; i < parameters.size(); i++ )
		numberOfSimulations *= parameters[i].numberOfValues();
}

void ParameterCreator::createSimpleSetOfSimulations()
{
	std::vector<size_t> valueIndexes;
	for ( size_t i = 0; i < parameters.size(); i++ )
		valueIndexes.push_back(0);
	
	calculateNumberOfSimulations();

	for ( size_t simulationIndex = 0; simulationIndex < numberOfSimulations; simulationIndex++ )
	{
		Simulation sim;
		// Cast in next line to solve apparent bug in overload of to_string
		std::string nameOfResultDirectory = this->name + "_" + std::to_string((long long int)simulationIndex);
		sim.setNameOfResultsDirectory( nameOfResultDirectory );
		sim.setLaunchSimulationCommand( "oarsub -S ./job.sh" ); // TO BE ADAPTED
		outputParameter.addValueForMTC(nameOfResultDirectory);

		// This loop assigns the values of the parameters to the a simulation
		for ( size_t parameterIndex = 0; parameterIndex < parameters.size(); parameterIndex++ )	
			sim.addValueOfParameter(parameters[parameterIndex].getValue(valueIndexes[parameterIndex]));

		simulations.push_back(sim);	
		augmentValueIndexes ( valueIndexes, parameters.size() - 1 );
	}
	
	/*Parameter parameter = parameters[0];
	for ( size_t i = 0; i < parameter.numberOfValues(); i++)
	{
		std::string value = parameter.getValue(i);
		Simulation sim;
		// Cast in next line to solve apparent bug in overload of to_string
		std::string nameOfResultDirectory = this->name + "_" + std::to_string((long long int)i);
		sim.setNameOfResultsDirectory( nameOfResultDirectory );
		sim.setLaunchSimulationCommand( "oarsub -S ./job.sh" ); // TO BE ADAPTED

		sim.addValueOfParameter(value);

		outputParameter.addValueForMTC(nameOfResultDirectory);

		simulations.push_back(sim);	
	}*/
}

void ParameterCreator::setOutputParameter(ResultsDirectoryParameter outputParameter)
{
	this-> outputParameter = outputParameter;
}


void ParameterCreator::writeReport() 
{	
	nameOfReportFile = "report"; // TO BE SET VIA CONF FILE
	std::ofstream reportOutput;
	reportOutput.open(nameOfReportFile.c_str());
        if (reportOutput.fail())
        {
                std::cerr << "Error: Report file " << nameOfReportFile << " could not be opened." << std::endl;
                exit (EXIT_FAILURE);
        }

	reportOutput << "nameOfParameter";
	for ( int paramIndex = 0; paramIndex < parameters.size(); paramIndex++ ) 
		reportOutput << " " << parameters[paramIndex].getName();
	reportOutput << std::endl;

	for ( int simIndex = 0; simIndex < simulations.size(); simIndex++ )
	{
		reportOutput << simulations[simIndex].getNameOfResultsDirectory();
		for ( int valIndex = 0; valIndex < simulations[simIndex].getValues().size(); valIndex++ ) 
			reportOutput << " " << simulations[simIndex].getValues()[valIndex];
		reportOutput << std::endl; 
	}
}


void ParameterCreator::launchSimulations(void) const
{
	int waitTimeInMinutes = waitTimeInMinutesBetweenBatches;
	size_t numberOfSimultaneousSimulations = 5;

	size_t currentNumberOfSimulations = 0;
	
	while ( currentNumberOfSimulations < simulations.size() )
	{
		size_t indexPerBatch = 0;
		while ( currentNumberOfSimulations < simulations.size()
		and indexPerBatch < numberOfSimultaneousSimulations )
		{
			launchSimulation(currentNumberOfSimulations);
			currentNumberOfSimulations++,	
			indexPerBatch++;
		}
	
		if ( currentNumberOfSimulations < simulations.size() )
			std::this_thread::sleep_for(std::chrono::minutes(waitTimeInMinutes));
		
	}

}

void ParameterCreator::launchSimulation(const int& simIndex) const
{
	// Modifying all files with parameters
	for ( size_t paramIndex = 0;  paramIndex < parameters.size(); paramIndex++ )
	{
		parameters[paramIndex].affectParameterValueInTargetFile(simulations[simIndex].getValueOfParameter(paramIndex));
	}

	// Setting up output file
	outputParameter.affectParameterValueInTargetFile(outputParameter.getValue(simIndex));

	// Creating results directory
	system(("mkdir " + simulations[simIndex].getNameOfResultsDirectory()).c_str());

	// Launching simulation	
	simulations[simIndex].launch();
	
	// Waiting a bit to make sure the mtc scripts were read
	int waitTimeInSeconds = (int)(60*waitTimeInMinutesBetweenSimulations + 0.5);
	std::this_thread::sleep_for(std::chrono::seconds(waitTimeInSeconds));
}

void ParameterCreator::setNameOfParametricStudy(const std::string& name)
{
	this->name = name;
}
