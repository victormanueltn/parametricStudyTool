#include "Simulation.h"

Simulation::Simulation(void)  
{
}

Simulation::~Simulation(void)
{
}

std::string Simulation::getValueOfParameter(const int& i) const
{
	return values[i];
}

void Simulation::addValueOfParameter(const std::string& value)
{
	values.push_back(value);
}

void Simulation::setNameOfResultsDirectory(const std::string& name)
{
	nameOfResultsDirectory = name;
}

std::vector<std::string> Simulation::getValues() const
{
	return values;
}

void Simulation::setLaunchSimulationCommand(const std::string& command)
{
	launchSimulationCommand = command;
}

void Simulation::launch() const
{
	system(launchSimulationCommand.c_str());
}

std::string Simulation::getNameOfResultsDirectory(void) const
{
	return nameOfResultsDirectory;
}


