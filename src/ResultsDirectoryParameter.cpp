#include "ResultsDirectoryParameter.h"
#include <cstdlib>

ResultsDirectoryParameter::ResultsDirectoryParameter(void) 
{                          
}                          
                           
                           
ResultsDirectoryParameter::~ResultsDirectoryParameter(void)
{                          
}                          

ResultsDirectoryParameter::ResultsDirectoryParameter(const std::string& parameterFile)
: Parameter(parameterFile)
{
	findLineNumber(nameOfOriginalFile);
}

void ResultsDirectoryParameter::findLineNumber(const std::string& nameOfOriginalFile)
{                                                                                               
        std::ifstream originalFile;                                                             
        openFile(originalFile, nameOfOriginalFile);                                             
                                                                                                
        std::string line;                                                                       
        size_t position = std::string::npos;                                                       
        size_t currentLineNumber = 0;                                                              
	bool foundAMatch = false;
        while ( getline( originalFile, line ) )                                                 
        {                                                                                       
                position = line.find(this->name);                                               
		bool localFoundAMatch = position != std::string::npos;
                if (localFoundAMatch)                                              
                {                                                                               
			lineNumbers.push_back(currentLineNumber);
			initialValues.push_back(line);
			position = std::string::npos;
			foundAMatch = foundAMatch or localFoundAMatch; 
                        //lineNumber = currentLineNumber;                                         
                        //initialValue = line;                                                    
                }                                                                               
                currentLineNumber++;                                                            
        }                                                                                       
        if ( not foundAMatch )                                                    
        {                                                                                       
                std::cerr << "Error: parameter " << name << " not found in file: " << std::endl;
                std::cerr << nameOfOriginalFile << std::endl;                                   
                exit (EXIT_FAILURE);                                                            
        }                                                                                       
}                                                   


void ResultsDirectoryParameter::addValueForMTC(const std::string& fileName)
{
	listOfValues.push_back( fileName );
	
}

bool replace(std::string& str, const std::string& from, const std::string& to)
	{
    		size_t start_pos = str.find(from);
    		if(start_pos == std::string::npos)
			return false;
		str.replace(start_pos, from.length(), to);
		return true;
	}



void ResultsDirectoryParameter::affectParameterValueInTargetFile(const std::string& value) const
{
	
                std::ifstream originalFile;
                std::ofstream targetFile;
		std::string valueToBePassed;

                openFile(originalFile, nameOfOriginalFile);
                openFile(targetFile, nameOfTargetFile);

                std::string line;
                int currentLineNumber = 0;
                while ( getline( originalFile, line ) )
                {
			bool matchInLineNumber = false;
			for( size_t i = 0; i < lineNumbers.size(); i++ )
			{
				matchInLineNumber = lineNumbers[i] == currentLineNumber;
				if ( matchInLineNumber ) break;
			}	
                        if ( matchInLineNumber )
			{
				valueToBePassed = "./"+value;
                                //targetFile << value << std::endl;
                                std::string currentInitialValue = line;
				std::cout << "Just before replace." << std::endl;
                                valueToBePassed = replace(currentInitialValue,name,valueToBePassed); 
				std::cout << "RRR " << currentInitialValue << std::endl;
                                targetFile << currentInitialValue << std::endl;
			}
                        else
                                targetFile << line << std::endl;

                        currentLineNumber++;
                }

                originalFile.close();
                targetFile.close();
}                                            
