#pragma once
#include <string>
#include <vector>
#include <ios>
#include <iostream>
#include <fstream>
#include <algorithm>


class Parameter
{
private:
	int lineNumber; /**< line number of parameter in nameOfFile */
	std::string initialValue; /**< Initial value in the original file */
protected:
	std::string name; /**< name of the parameter */
	std::string nameOfOriginalFile; /**< name of the file including path */
	std::string nameOfTargetFile; /**< name of file(s) where parameter will take the values in listOfValues */
	std::vector<std::string> listOfValues; /**< Values to be affected to parameter in file(s) targetFile  */

	/** Finds out the parameter's line number in file nameOfOriginalFile */
	virtual void findLineNumber(const std::string& nameOfOriginalFile);

	/** Opens input file and checks that it opened correctly */
	void openFile( std::ifstream& stream, const std::string& fileName ) const;
	/** Opens output file and checks that it opened correctly */
	void openFile( std::ofstream& stream, const std::string& fileName ) const;

public:
	Parameter(void);
	~Parameter(void);

	/** Constructor based on file name */
	Parameter( const std::string& parameterFile );

	/** Modifies target file with the value passed as parameter */ 
	void affectParameterValueInTargetFile(const std::string& value) const;
	std::string getValue(const int& i) const;

	int numberOfValues() const;

	std::string getName() const;
	

};

