#pragma once
#include <string>
#include <vector>
#include <cstdlib>
#include <iostream>

/** A simulation is represented by the values of each of its parameters */
class Simulation
{
public:
	Simulation(void);
	~Simulation(void);
	std::string getValueOfParameter(const int& i) const;
	void addValueOfParameter(const std::string& value);
	void setNameOfResultsDirectory(const std::string& name);
	void setLaunchSimulationCommand(const std::string& command);
	std::string getNameOfResultsDirectory(void) const;
	void launch(void) const;
	std::vector<std::string> getValues() const;
protected:
	/** List of values of parameters. Value i corresponds to parameter i */
	std::vector<std::string> values;
	std::string nameOfResultsDirectory;
	/** String containing the command that will be used to launch the simulation */
	std::string launchSimulationCommand;
	/** List of line numbers to be changed to respect output in resultsDirectory */ 
	std::vector <int> lineNumbersForOutputDirectory;
};
