#pragma once
#define _GLIBCXX_USE_NANOSLEEP // I had to this in order to use sleep_for due to an apparent problem with compiler
#include <vector>
#include <thread>
#include <chrono>
#include <string>
#include <cstdlib>
#include "Parameter.h"
#include "ResultsDirectoryParameter.h"
#include "Simulation.h"
#include <iostream>
#include <fstream>

class ParameterCreator
{
public:
	ParameterCreator(void);
	ParameterCreator( const std::string& configFile );
	~ParameterCreator(void);
	void addParameter(Parameter p); /**< Adds a new parameter */
	void createSimpleSetOfSimulations(); /**< Creates a set of simulations based on the existing parameters. */
	void setOutputParameter(ResultsDirectoryParameter outputParameter);
/** Writes a report containing the list of the generated simulations with their names and the value of each of the parameters */
	void writeReport() ; // TO BE IMPLEMENTED, CHANGE TO CONST LATER
	/** Reads configuration file that specifies name of the parametric study, launchSimulationCommand, number of simultaneous simulations, wait time between batches, short wait time between simulations of the same batch, path of parameter files to be used, name of the report file */
	void readConfigurationFile(); // TO BE IMPLEMENTED
	/** Launches all simulations */ 
	void launchSimulations(void) const;
	/** Launches the simulation corresponding to the index simIndex */
	void launchSimulation(const int& simIndex) const;
	void setNameOfParametricStudy(const std::string& name);
protected:
	/** List of parameters */
	std::vector<Parameter> parameters;
	/** List of simulations */
	std::vector<Simulation> simulations;
	/** Number of simulations */
	size_t numberOfSimulations;
	/** Name of parametric study */
	std::string name;
	/** Name of report file */
	std::string nameOfReportFile;
	/** Parameter to manage output file */
	ResultsDirectoryParameter outputParameter;
        double waitTimeInMinutesBetweenBatches;
        double waitTimeInMinutesBetweenSimulations;
	void openFile ( std::ifstream& stream, const std::string& fileName ) const;
	/** Increments the indexes of the values respecting the number of values of each parameter */
	void augmentValueIndexes( std::vector<size_t>& valueIndexes, const int& parameterIndex ) const;
	/** Calculates total number of simulations */
	void calculateNumberOfSimulations();
	
};

