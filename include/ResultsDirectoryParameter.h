#pragma once
#include <vector>
#include <string>
#include "Parameter.h"

class ResultsDirectoryParameter : public Parameter
{
protected:
       // std::string name; 
       // std::string nameOfOriginalFile; 
       /** List containing the lineNumbers of the initial values in the original file */
       std::vector<int> lineNumbers;
       // std::string nameOfTargetFile;
       // std::vector<std::string> listOfValues;
       /** List containing the initial value(s) in the original file */
       std::vector<std::string> initialValues;

        /** Finds out the parameter's line number in file nameOfOriginalFile */
        virtual void findLineNumber(const std::string& nameOfOriginalFile);

        //void openFile( std::ifstream& stream, const std::string& fileName );
        //void openFile( std::ofstream& stream, const std::string& fileName );

public:
        ResultsDirectoryParameter(void);
        ~ResultsDirectoryParameter(void);
	ResultsDirectoryParameter(const std::string& parameterFile);

	void addValueForMTC(const std::string& fileName);

        /** Modifies target file with the value of the parameter of the component
          corresponding to valueIndex in listOfValues */
        void affectParameterValueInTargetFile(const std::string& value) const;

        // std::string getValue(const int& i) const;

        int numberOfValues() const;


#include <vector>
};
